INSERT INTO login_info (block_account_after)
	SELECT '5' FROM DUAL
WHERE NOT EXISTS
  (SELECT id FROM login_info WHERE id=1);
INSERT INTO login_info (block_account_after)
	SELECT '5' FROM DUAL
WHERE NOT EXISTS
  (SELECT id FROM login_info WHERE id=2);
INSERT INTO login_info (block_account_after)
	SELECT '5' FROM DUAL
WHERE NOT EXISTS
  (SELECT id FROM login_info WHERE id=31);
INSERT INTO login_info (block_account_after)
	SELECT '5' FROM DUAL
WHERE NOT EXISTS
  (SELECT id FROM login_info WHERE id=4);
INSERT INTO login_info (block_account_after)
	SELECT '5' FROM DUAL
WHERE NOT EXISTS
  (SELECT id FROM login_info WHERE id=5);

INSERT INTO users (nick,password,role,login_info_id)
	SELECT 'admin','admin','ADMIN','1' FROM DUAL
WHERE NOT EXISTS
  (SELECT nick FROM users WHERE nick='admin');
INSERT INTO users (nick,password,role,login_info_id)
	SELECT 'user','user','ADMIN','2' FROM DUAL
WHERE NOT EXISTS
  (SELECT nick FROM users WHERE nick='user');
INSERT INTO users (nick,password,role,login_info_id)
	SELECT 'user1','user1','ADMIN','3' FROM DUAL
WHERE NOT EXISTS
  (SELECT nick FROM users WHERE nick='user1');
INSERT INTO users (nick,password,role,login_info_id)
	SELECT 'user2','user2','ADMIN','4' FROM DUAL
WHERE NOT EXISTS
  (SELECT nick FROM users WHERE nick='user2');
INSERT INTO users (nick,password,role,login_info_id)
	SELECT 'user3','user3','ADMIN','5' FROM DUAL
WHERE NOT EXISTS
  (SELECT nick FROM users WHERE nick='user3');


INSERT INTO messages (contents, owner_id)
    SELECT 'aaaa','2' from DUAL
WHERE NOT EXISTS
    (SELECT id FROM messages where id=1);
INSERT INTO messages (contents, owner_id)
    SELECT 'bsfsdf','1' from DUAL
WHERE NOT EXISTS
    (SELECT id FROM messages where id=2);
INSERT INTO messages (contents, owner_id)
    SELECT 'sdfxcvs','4' from DUAL
WHERE NOT EXISTS
    (SELECT id FROM messages where id=3);
INSERT INTO messages (contents, owner_id)
    SELECT 'sdfasdasdxcvs','4' from DUAL
WHERE NOT EXISTS
    (SELECT id FROM messages where id=4);
INSERT INTO messages (contents, owner_id)
    SELECT 'sdfxaaaaacvs','2' from DUAL
WHERE NOT EXISTS
    (SELECT id FROM messages where id=5);
INSERT INTO messages (contents, owner_id)
    SELECT 'sdaaaafxcvs','3' from DUAL
WHERE NOT EXISTS
    (SELECT id FROM messages where id=6);
INSERT INTO messages (contents, owner_id)
    SELECT 'sffffdfxcvs','4' from DUAL
WHERE NOT EXISTS
    (SELECT id FROM messages where id=7);
INSERT INTO messages (contents, owner_id)
    SELECT 'sdrr$$fxcvs','1' from DUAL
WHERE NOT EXISTS
    (SELECT id FROM messages where id=8);
INSERT INTO messages (contents, owner_id)
    SELECT 'sdfx$$$cvs','1' from DUAL
WHERE NOT EXISTS
    (SELECT id FROM messages where id=9);
INSERT INTO messages (contents, owner_id)
    SELECT 'sdfx$$$cvs','2' from DUAL
WHERE NOT EXISTS
    (SELECT id FROM messages where id=10);


INSERT INTO users_messages (users_id, messages_id)
    SELECT '2','1' from DUAL
WHERE NOT EXISTS
    (SELECT messages_id FROM users_messages where messages_id=1);
INSERT INTO users_messages (users_id, messages_id)
    SELECT '1','2' from DUAL
WHERE NOT EXISTS
    (SELECT messages_id FROM users_messages where messages_id=2);
INSERT INTO users_messages (users_id, messages_id)
    SELECT '4','3' from DUAL
WHERE NOT EXISTS
    (SELECT messages_id FROM users_messages where messages_id=3);
INSERT INTO users_messages (users_id, messages_id)
    SELECT '4','4' from DUAL
WHERE NOT EXISTS
    (SELECT messages_id FROM users_messages where messages_id=4);
INSERT INTO users_messages (users_id, messages_id)
    SELECT '2','5' from DUAL
WHERE NOT EXISTS
    (SELECT messages_id FROM users_messages where messages_id=5);
INSERT INTO users_messages (users_id, messages_id)
    SELECT '3','6' from DUAL
WHERE NOT EXISTS
    (SELECT messages_id FROM users_messages where messages_id=6);
INSERT INTO users_messages (users_id, messages_id)
    SELECT '5','7' from DUAL
WHERE NOT EXISTS
    (SELECT messages_id FROM users_messages where messages_id=7);
INSERT INTO users_messages (users_id, messages_id)
    SELECT '1','8' from DUAL
WHERE NOT EXISTS
    (SELECT messages_id FROM users_messages where messages_id=8);
INSERT INTO users_messages (users_id, messages_id)
    SELECT '1','9' from DUAL
WHERE NOT EXISTS
    (SELECT messages_id FROM users_messages where messages_id=9);
INSERT INTO users_messages (users_id, messages_id)
    SELECT '2','10' from DUAL
WHERE NOT EXISTS
    (SELECT messages_id FROM users_messages where messages_id=10);
