/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.service;

import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pb.bai.ps2.model.entity.LoginInfo;
import pb.bai.ps2.model.entity.User;
import pb.bai.ps2.model.repository.LoginInfoRepository;

/**
 *
 * @author Maciek
 */
@Service
public class LoginInfoServiceImpl implements LoginInfoService {

    private final UserService userService;
    private final LoginInfoRepository loginInfoRepository;

    @Autowired
    public LoginInfoServiceImpl(UserService userService, LoginInfoRepository loginInfoRepository) {
        this.userService = userService;
        this.loginInfoRepository = loginInfoRepository;
    }

    @Override
    public LoginInfo getLoginInfoByUserId(Integer userId) {
        Optional<User> o = userService.getUserById(userId);
        if (o.isPresent()) {
            return o.get().getLoginInfo();
        }
        return null;
    }

    @Override
    public LoginInfo save(LoginInfo logininfo) {
        return loginInfoRepository.save(logininfo);
    }

    @Override
    public long isBlocked(int userId) {
        Optional<User> user = userService.getUserById(userId);
        if (user.isPresent() && user.get().getLoginInfo().getBlockedTo() != null) {
            return (user.get().getLoginInfo().getBlockedTo().getTime() - new Date().getTime()) / 1000;
        } else {
            return 0;
        }
    }
}
