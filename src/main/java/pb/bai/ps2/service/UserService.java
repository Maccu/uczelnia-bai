/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.service;

import java.util.Collection;
import java.util.Optional;

import pb.bai.ps2.model.entity.Message;
import pb.bai.ps2.model.entity.User;

/**
 *
 * @author Maciek
 */
public interface UserService {
    Optional<User> getUserById(int id);
    Optional<User> getUserName(String name);
    Collection<User> getAllUsers();
    User update(User user);
    void removeMessage(User user, Message message);
    void blockPernamently(int userId);
    void unBlockPernamently(int userId);
//    User create(UserCreateForm form);

}
