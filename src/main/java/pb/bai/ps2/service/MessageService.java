/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.service;

import pb.bai.ps2.model.entity.Message;

import java.util.Collection;
import java.util.Optional;
import pb.bai.ps2.model.entity.User;


/**
 * Created by niewinskip on 2016-10-23.
 */
public interface MessageService {
    Optional<Message> getMessageById(int id);

    Collection<Message> getAllMessages();
    Message update(Message message);
    Message create(Message message);
    Boolean isOwner(User user, Message message);
    Boolean canEdit(User user, Message message);
    void delete(Message message);
}
