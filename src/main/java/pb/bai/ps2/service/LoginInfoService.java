/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.service;

import pb.bai.ps2.model.entity.LoginInfo;

/**
 *
 * @author Maciek
 */
public interface LoginInfoService {
    LoginInfo getLoginInfoByUserId(Integer userId);
    LoginInfo save(LoginInfo logininfo);
    long isBlocked(int userId);
}
