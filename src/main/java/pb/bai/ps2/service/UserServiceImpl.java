/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pb.bai.ps2.model.entity.Message;
import pb.bai.ps2.model.entity.User;
import pb.bai.ps2.model.repository.UserRepository;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Optional;

/**
 *
 * @author ziniewiczm
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> getUserById(int id) {
        return Optional.ofNullable(userRepository.findOne(id));
    }

    @Override
    public Optional<User> getUserName(String name) {
        if (name == null || name.equalsIgnoreCase("anonymousUser")) {
            User u = new User();
            u.setNick("anonymous");
            return Optional.ofNullable(u);
        }
        return userRepository.getUserByNick(name);
    }

    @Override
    public Collection<User> getAllUsers() {
        Iterable<User> users = userRepository.findAll();
        Iterator<User> usersIterator = users.iterator();
        Collection<User> usersOut = new LinkedList<>();
        while (usersIterator.hasNext()) {
            User u = usersIterator.next();
            usersOut.add(u);
        }
        return usersOut;
    }

    @Override
    public void removeMessage(User user, Message message) {
        int id = -1;
        for (int x = 0; x < user.getMessages().size(); x++) {
            if (message.getId() == user.getMessages().get(x).getId()) {
                id = x;
                break;
            }
        }
        if (id != -1) {
            user.getMessages().remove(id);
        }
    }

    @Override
    public User update(User user) {
        return userRepository.save(user);
    }

//        @Override
//    public User create(UserCreateForm form) {
//        User user = new User();
//        user.setEmail(form.getEmail());
//        user.setPasswordHash(new BCryptPasswordEncoder().encode(form.getPassword()));
//        user.setRole(form.getRole());
//        return userRepository.save(user);
//    }
//
    @Override
    public void blockPernamently(int userId) {
        Optional<User> user = getUserById(userId);
        if (user.isPresent()) {
            User u = user.get();
            u.setPernamentlyBlock(true);
            update(u);
        }
    }

    @Override
    public void unBlockPernamently(int userId) {
        Optional<User> user = getUserById(userId);
        if (user.isPresent()) {
            user.get().setPernamentlyBlock(false);
            update(user.get());
        }
    }
}
