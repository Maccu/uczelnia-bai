package pb.bai.ps2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pb.bai.ps2.model.entity.Message;
import pb.bai.ps2.model.entity.User;
import pb.bai.ps2.model.repository.MessageRepository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import pb.bai.ps2.model.entity.User;

/**
 * Created by niewinskip on 2016-10-23.
 */
@Service
public class MessageServiceImpl implements MessageService {
    
    private final MessageRepository msgRepository;
    
    @Autowired
    public MessageServiceImpl(MessageRepository msgRepository) {
        this.msgRepository = msgRepository;
    }
    
    @Override
    public Optional<Message> getMessageById(int id) {
        return Optional.ofNullable(msgRepository.findOne(id));
    }
    
    @Override
    public Collection<Message> getAllMessages() {
        Iterable<Message> messages = msgRepository.findAll();
        Iterator<Message> messageIterator = messages.iterator();
        Collection<Message> msgOut = new LinkedList<>();
        while (messageIterator.hasNext()) {
            Message m = messageIterator.next();
            msgOut.add(m);
        }
        return msgOut;
    }
    
    @Override
    public Message create(Message message) {
        if (message.getContents() == null || message.getContents().isEmpty()) {
            message.setContents("default message");
        }
        return msgRepository.save(message);
    }
    
    @Override
    public Message update(Message message) {
         Map<Integer, User> usersMap = new HashMap<>();
        for (Iterator<User> it = message.getUsersCanModify().iterator(); it.hasNext();) {
            User utemp = it.next();
            usersMap.put(utemp.getId(), utemp);
        }
        message.getUsersCanModify().clear();
        for (Map.Entry<Integer, User> entry : usersMap.entrySet()) {
            message.getUsersCanModify().add(entry.getValue());
        }
        return msgRepository.save(message);
    }
    
    @Override
    public Boolean isOwner(User user, Message message) {
        return message.getOwner().getId() == user.getId();
    }
    
    @Override
    public Boolean canEdit(User user, Message message) {
        if (isOwner(user, message)) {
            return true;
        }
        for (User canEdit : message.getUsersCanModify()) {
            if (canEdit.getId() == user.getId()) {
                return true;
            }
        }
        return false;
    }
    
    public void delete(Message message) {
        message.setUsersCanModify(new LinkedList<>());
        message.setOwner(null);
        update(message);
        msgRepository.delete(message);
    }
    
}
