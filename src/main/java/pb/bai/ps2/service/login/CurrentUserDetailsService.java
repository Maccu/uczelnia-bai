/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.service.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pb.bai.ps2.model.entity.User;
import pb.bai.ps2.service.UserService;

/**
 *
 * @author ziniewiczm
 */
@Service
public class CurrentUserDetailsService implements UserDetailsService {
    private final UserService userService;

    @Autowired
    public CurrentUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CurrentUser loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = userService.getUserName(name)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User with name=%s was not found", name)));
        return new CurrentUser(user);
    }
}
