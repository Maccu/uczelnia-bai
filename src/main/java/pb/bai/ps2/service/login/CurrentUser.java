/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.service.login;

import org.springframework.context.annotation.Role;
import org.springframework.security.core.authority.AuthorityUtils;
import pb.bai.ps2.model.entity.User;

/**
 *
 * @author ziniewiczm
 */
public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private User user;

    public CurrentUser(User user) {
        super(user.getNick(), user.getPassword(), AuthorityUtils.createAuthorityList(user.getRole().toString()));
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Integer getId() {
        return user.getId();
    }

}