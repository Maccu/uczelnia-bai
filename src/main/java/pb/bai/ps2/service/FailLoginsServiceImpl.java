/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pb.bai.ps2.model.entity.FailLogins;
import pb.bai.ps2.model.repository.FailLoginsRepository;

/**
 *
 * @author Maciek
 */
@Service
public class FailLoginsServiceImpl {
    private FailLoginsRepository flr;

    @Autowired
    public FailLoginsServiceImpl(FailLoginsRepository flr) {
        this.flr = flr;
    }
    
    public FailLogins save(FailLogins fl){
        return flr.save(fl);
    }
    
}
