/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.configuration;

import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import pb.bai.ps2.model.entity.LoginInfo;
import pb.bai.ps2.model.entity.User;
import pb.bai.ps2.service.LoginInfoService;
import pb.bai.ps2.service.UserService;

/**
 *
 * @author Maciek
 */
@Component
public class LimitLoginAuthenticationProvider extends DaoAuthenticationProvider {

    private int limitLogins;
    private Date blockDate = new Date();

    @Autowired
    @Override
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        super.setUserDetailsService(userDetailsService);
    }

    private final LoginInfoService loginInfoService;
    private final UserService userService;

    @Autowired
    public LimitLoginAuthenticationProvider(LoginInfoService loginInfoService, UserService userService) {
        this.loginInfoService = loginInfoService;
        this.userService = userService;
    }

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        try {
            System.out.println(getLockTime());
            if (getLockTime() > 0) {
                throw new LockedException("Log in access is blocked for: " + getLockTime() + " secs");
            }
            Optional<User> u = userService.getUserName(authentication.getName());
            if (u.isPresent() && u.get().isPernamentlyBlock()) {
                throw new LockedException("Yor account is pernamently block");
            }
            Authentication auth = super.authenticate(authentication);
            limitLogins = 0;
            return auth;
        } catch (BadCredentialsException e) {
            limitLogins++;
            blockLogin();
            Optional<User> user = userService.getUserName(authentication.getName());
            if (user.isPresent() && user.get().getLoginInfo().getLoginFailsSinceLastSuccess() < user.get().getLoginInfo().getBlockAccountAfter()-1) {
//                throw new BadCredentialsException(e.getMessage() + ". You have left "
//                        + (user.get().getLoginInfo().getBlockAccountAfter() - user.get().getLoginInfo().getLoginFailsSinceLastSuccess()) + " until your account will be blocked");
                    LoginInfo li = user.get().getLoginInfo();
                    li.setLoginFailsSinceLastSuccess(li.getLoginFailsSinceLastSuccess()+1);
                    li.setLastFailureLogin(new Date());
                    loginInfoService.save(li);
            }
            if (user.isPresent() && user.get().getLoginInfo().getLoginFailsSinceLastSuccess() >= user.get().getLoginInfo().getBlockAccountAfter() -1) {
                userService.blockPernamently(user.get().getId());
            }
            throw e;
        } catch (LockedException e) {
            throw e;
        }
    }

    private void blockLogin() {
        long time = new Date().getTime();
        time = (time / 1000) + limitLogins * 5;
        blockDate = new Date(time * 1000);
    }

    private long getLockTime() {
        return (blockDate.getTime() - new Date().getTime()) / 1000;
    }

    public long isBlocked(int userId) {
        Optional<User> user = userService.getUserById(userId);
        if (user.isPresent() && user.get().getLoginInfo().getBlockedTo() != null) {
            return (user.get().getLoginInfo().getBlockedTo().getTime() - new Date().getTime()) / 1000;
        } else {
            return 0;
        }
    }

}
