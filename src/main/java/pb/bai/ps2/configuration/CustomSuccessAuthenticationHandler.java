/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.configuration;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import pb.bai.ps2.model.entity.LoginInfo;
import pb.bai.ps2.model.entity.User;
import pb.bai.ps2.service.LoginInfoService;
import pb.bai.ps2.service.UserService;

/**
 *
 * @author Maciek
 */
@Component
public class CustomSuccessAuthenticationHandler implements AuthenticationSuccessHandler {

    private final LoginInfoService loginInfoService;
    private final UserService userService;

    @Autowired
    public CustomSuccessAuthenticationHandler(LoginInfoService loginInfoService, UserService userService) {
        this.loginInfoService = loginInfoService;
        this.userService = userService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        Optional<User> o = userService.getUserName(authentication.getName());
        if(o.isPresent()){
            LoginInfo li = o.get().getLoginInfo();
            li.setLoginFailsSinceLastSuccess(0);
            li.setLastSuccesLogin(li.getActualSuccesLogin());
            li.setActualSuccesLogin(new Date());
            loginInfoService.save(li);
        }
        response.sendRedirect("/all-msg");
//        redirectStrategy.sendRedirect(request, response, );
//        handle(request, response, authentication);
//        clearAuthenticationAttributes(request);
    }

}
