package pb.bai.ps2.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by ziniewiczm on 27.09.2016.
 */
@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomFailureAuthenticationHandler customFailureAuthenticationHandler;
    @Autowired
    CustomSuccessAuthenticationHandler customSuccessAuthenticationHandler;
    @Autowired
    private LimitLoginAuthenticationProvider limitLoginAuthenticationProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/login", "/all-msg").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().loginPage("/login")
                .usernameParameter("username").successHandler(customSuccessAuthenticationHandler).failureHandler(customFailureAuthenticationHandler)
                .permitAll().and().logout().logoutUrl("/logout")
                .logoutSuccessUrl("/all-msg").permitAll();

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(limitLoginAuthenticationProvider);
    }
}
