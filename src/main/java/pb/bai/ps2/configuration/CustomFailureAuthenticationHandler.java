/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.configuration;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import pb.bai.ps2.model.entity.FailLogins;
import pb.bai.ps2.service.FailLoginsServiceImpl;
import pb.bai.ps2.service.UserService;

/**
 *
 * @author Maciek
 */
@Component
public class CustomFailureAuthenticationHandler implements AuthenticationFailureHandler {

    private FailLoginsServiceImpl failLoginsServiceImpl;
    @Autowired
    private UserService userService;

    @Autowired
    public CustomFailureAuthenticationHandler(FailLoginsServiceImpl failLoginsServiceImpl) {
        this.failLoginsServiceImpl = failLoginsServiceImpl;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        failLoginsServiceImpl.save(new FailLogins(request.getParameter("username"), request.getParameter("password")));
        response.sendRedirect("/login?error="+exception.getMessage());
    }
}
