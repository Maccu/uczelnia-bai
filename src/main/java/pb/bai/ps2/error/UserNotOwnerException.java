/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.error;

/**
 *
 * @author Maciek
 */
public class UserNotOwnerException extends Exception {

    public UserNotOwnerException() {
        super();
    }

    public UserNotOwnerException(String message) {
        super(message);
    }
}
