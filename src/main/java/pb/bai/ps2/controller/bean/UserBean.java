/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.controller.bean;

/**
 *
 * @author Maciek
 */
public class UserBean {
    private int id;
    private String nick;

    public UserBean(int id, String nick) {
        this.id = id;
        this.nick = nick;
    }

    public UserBean() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
    
    
    
}
