/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.controller.bean;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import pb.bai.ps2.model.entity.User;

/**
 *
 * @author Maciek
 */
public class MessageBean {

    private int id;
    private String contents;
    List<UserBean> users;

    public MessageBean() {
    }

    public MessageBean(int id, String contents) {
        this.id = id;
        this.contents = contents;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public List<UserBean> getUsers() {
        return users;
    }

    public void setUsers(List<User> usersList) {
        users = new LinkedList<UserBean>();
         Map<Integer, UserBean> usersMap = new HashMap<>();
        for (Iterator<User> it = usersList.iterator(); it.hasNext();) {
            User utemp = it.next();
            usersMap.put(utemp.getId(), new UserBean(utemp.getId(),utemp.getNick()));
        }
        for (Entry<Integer, UserBean> entry : usersMap.entrySet()) {
            users.add(entry.getValue());
        }
//        for (User u : usersList) {
//            users.add(new UserBean(u.getId(), u.getNick()));
//        }
    }
}
