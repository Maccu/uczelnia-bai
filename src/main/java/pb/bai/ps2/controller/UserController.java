package pb.bai.ps2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pb.bai.ps2.model.entity.LoginInfo;
import pb.bai.ps2.model.entity.User;
import pb.bai.ps2.service.LoginInfoService;
import pb.bai.ps2.service.UserService;

/**
 * Created by ziniewiczm on 27.09.2016.
 */
@Controller
public class UserController {

    private final UserService userService;
    private final LoginInfoService infoService;

    @Autowired
    public UserController(UserService userService,
            LoginInfoService infoService) {
        this.userService = userService;
        this.infoService = infoService;
    }

    @RequestMapping(value = {"/userview"})
    public String greeting(Model model) {
        User loggedUser = getLoggedUser();
        model.addAttribute("loggedUser", loggedUser);
        return "userview";
    }

    @RequestMapping(value = {"/setFailsLoginNumber"})
    public @ResponseBody
    LoginFails greeting(@RequestParam(value = "number", required = true) Integer number, Model model) {
        User loggedUser = getLoggedUser();
        LoginInfo li = loggedUser.getLoginInfo();
        if (number < 1) {
            return new LoginFails(li.getBlockAccountAfter(),"Value cannot be below 1");
        }
        li.setBlockAccountAfter(number);
        infoService.save(li);
        return new LoginFails(li.getBlockAccountAfter(),"");
    }

    @RequestMapping("/login")
    public String login(Model model,
            @RequestParam(value = "error", required = false) String error) {
        model.addAttribute("error", error);
        return "login";
    }

    public User getLoggedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserName(auth.getName()).get();
        return user;
    }

    private class LoginFails{
        private int number;
        private String message;

        public LoginFails(int number, String message) {
            this.number = number;
            this.message = message;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
        
        
    }
}
