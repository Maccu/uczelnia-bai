package pb.bai.ps2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pb.bai.ps2.controller.bean.MessageBean;
import pb.bai.ps2.error.UserNotOwnerException;
import pb.bai.ps2.model.entity.Message;
import pb.bai.ps2.model.entity.User;
import pb.bai.ps2.model.repository.UserRepository;
import pb.bai.ps2.service.MessageService;
import pb.bai.ps2.service.UserService;
import java.util.Iterator;
import java.util.List;

/**
 * Created by niewinskip on 2016-10-22.
 */
@Controller
public class UserMessagesController {

    private final UserService us;
    private final MessageService messageService;
    private final UserRepository userRepository;

    @Autowired
    public UserMessagesController(UserService us, MessageService messageService, UserRepository userRepository) {
        this.us = us;
        this.messageService = messageService;
        this.userRepository = userRepository;
    }

    @RequestMapping(value = {"/all-msg", "", "/", "/index", "/home"})
    public String greeting(Model model) {
        User loggedUser = getLoggedUser();
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("messages", messageService.getAllMessages());
        return "allMsg";
    }

    @RequestMapping("/showmessagesview")
    public String display(@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("loggedUser", getLoggedUser());
        model.addAttribute("usermessages", initializeMap());
        model.addAttribute("messageService", messageService);
        return "showmessagesview";
    }

    @RequestMapping("/newmessageview")
    public String displayView(@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("loggedUser", getLoggedUser());
        return "newmessageview";
    }

    private List<Message> initializeMap() {
        List<Message> msgList = getLoggedUser().getMessages();
        for (Message msg : messageService.getAllMessages()) {
            if (msg.getOwner() == null) {
                continue;
            }
            if (msg.getOwner().getId() == getLoggedUser().getId()) {
                continue;
            }
            for (User user : msg.getUsersCanModify()) {
                if (user.getId() == getLoggedUser().getId()) {
                    msgList.add(msg);
                    break;
                }
            }
        }
        return msgList;
    }

    public User getLoggedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        auth.getName();
        User user = us.getUserName(auth.getName()).get();
        return user;
    }

    @RequestMapping("/createnewmessage")
    public String createMessage(@RequestParam(value = "newtext") String message, Model model) {
        System.out.println(message);
        Message m = new Message();
        m.setContents(message);
        m.setOwner(getLoggedUser());
        messageService.create(m);
        User user = getLoggedUser();
        user.addMessage(m);
        userRepository.save(user);
        return "redirect:/showmessagesview.html";
    }

    @RequestMapping(value = "/modifyAuth", method = RequestMethod.GET)
    public String addModifyAuth(Model model) {
        model.addAttribute("usermessages", getLoggedUser().getMessages());
        model.addAttribute("users", us.getAllUsers());
        model.addAttribute("loggedUser", getLoggedUser());
        return "modifyAuth";
    }

    @RequestMapping(value = "/addModifyAuth", method = RequestMethod.GET)
    public String addModifyAuth(@RequestParam("messageId") int messageId, @RequestParam("userId") int userId) throws UserNotOwnerException {
        Message m = messageService.getMessageById(messageId).get();
        if (!messageService.isOwner(getLoggedUser(), m)) {
            throw new UserNotOwnerException("You have no rights to permorm this action");
        }
        m.addUserCanModify(us.getUserById(userId).get());
        messageService.update(m);
        return "redirect:/modifyAuth.html";
    }

    @RequestMapping(value = "/editMessage", method = RequestMethod.GET)
    public @ResponseBody
    MessageBean editMessage(@RequestParam("messageId") int messageId, @RequestParam("contents") String contents) throws UserNotOwnerException {

        Message m = messageService.getMessageById(messageId).get();
        if (!messageService.canEdit(getLoggedUser(), m)) {
            throw new UserNotOwnerException("You have no rights to permorm this action");
        }
        m.setContents(contents);
        m = messageService.update(m);
        return new MessageBean(m.getId(), m.getContents());
    }

    @RequestMapping(value = "/getMessage", method = RequestMethod.GET)
    public @ResponseBody
    MessageBean removeAuth(@RequestParam("messageId") int messageId) throws UserNotOwnerException {
        Message m = messageService.getMessageById(messageId).get();
        if (!messageService.canEdit(getLoggedUser(), m)) {
            throw new UserNotOwnerException("You have no rights to permorm this action");
        }
        MessageBean mb = new MessageBean(m.getId(), m.getContents());
        mb.setUsers(m.getUsersCanModify());
        return mb;
    }

    @RequestMapping(value = "/removeAuth", method = RequestMethod.GET)
    public @ResponseBody
    MessageBean removeAuth(@RequestParam("messageId") int messageId, @RequestParam("userId") int userId) throws UserNotOwnerException {

        Message m = messageService.getMessageById(messageId).get();
        if (!messageService.isOwner(getLoggedUser(), m)) {
            throw new UserNotOwnerException("You have no rights to permorm this action");
        }
        Iterator<User> i = m.getUsersCanModify().iterator();
        while (i.hasNext()) {
            User u = i.next();
            if (u.getId() == userId) {
                i.remove();
            }
        }

        m = messageService.update(m);
        MessageBean mb = new MessageBean(m.getId(), m.getContents());
        mb.setUsers(m.getUsersCanModify());
        return mb;
    }

    @RequestMapping(value = "/deleteMessage", method = RequestMethod.GET)
    public String deleteMessage(@RequestParam("messageId") int messageId) throws UserNotOwnerException {
        Message m = messageService.getMessageById(messageId).get();
        if (m.getOwner().getId() != getLoggedUser().getId()) {
            throw new UserNotOwnerException("You have no rights to perform this action");
        }
        for (User user : us.getAllUsers()) {
            us.removeMessage(user, m);
            us.update(user);
        }
        m.setOwner(null);
        messageService.update(m);
        messageService.delete(m);
        return "redirect:/showmessagesview.html";
    }

    @ExceptionHandler(value = UserNotOwnerException.class)
    public String unoeHandler(UserNotOwnerException e) {
        return e.getMessage();
    }

}
