/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.model.entity;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.*;

/**
 *
 * @author Maciek
 */
@Entity
@Table(name = "messages")
public class Message {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String contents;
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @Cascade({org.hibernate.annotations.CascadeType.DELETE})
    private User owner;

    @OneToMany(targetEntity = User.class, fetch = FetchType.EAGER)
    @Cascade({org.hibernate.annotations.CascadeType.DELETE})
    private List<User> usersCanModify;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<User> getUsersCanModify() {

        return usersCanModify;
    }

    public void setUsersCanModify(List<User> usersCanModify) {
        this.usersCanModify = usersCanModify;
    }

    public void addUserCanModify(User user) {
        if (usersCanModify == null) {
            usersCanModify = new LinkedList<>();
        }
        usersCanModify.add(user);
        Map<Integer, User> usersMap = new HashMap<>();
        for (Iterator<User> it = usersCanModify.iterator(); it.hasNext();) {
            User utemp = it.next();
            usersMap.put(utemp.getId(), utemp);
        }
        usersCanModify.clear();
        for (Entry<Integer, User> entry : usersMap.entrySet()) {
            usersCanModify.add(entry.getValue());
        }
    }
}
