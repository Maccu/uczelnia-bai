/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.model.entity;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

/**
 *
 * @author Maciek
 */
@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true)
    private String nick;
    private String password;
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;
    @OneToMany(targetEntity = Message.class, fetch = FetchType.EAGER)
    @Cascade(CascadeType.DELETE)
    private List<Message> messages;
    @OneToOne(targetEntity = LoginInfo.class)
    private LoginInfo loginInfo;
    @Column(columnDefinition = "boolean default false")
    private boolean pernamentlyBlock;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void addMessage(Message message) {
        this.messages.add(message);
    }

    public void removeMessage(Message message) {
        int id = -1;
        for (int x = 0; x < messages.size(); x++) {
            if (message.getId() == message.getId()) {
                id = x;
                break;
            }
        }
        if (id != -1) {
            messages.remove(id);
        }
    }

    public LoginInfo getLoginInfo() {
        return loginInfo;
    }

    public void setLoginInfo(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }

    public boolean isPernamentlyBlock() {
        return pernamentlyBlock;
    }

    public void setPernamentlyBlock(boolean pernamentlyBlock) {
        this.pernamentlyBlock = pernamentlyBlock;
    }

    @Override
    public String toString() {
        return "id=" + id + ", nick=" + nick + ", password=" + password;
    }
}
