/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.model.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Maciek
 */
@Entity
@Table(name = "loginInfo")
public class LoginInfo {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date lastSuccesLogin;
    private Date actualSuccesLogin;
    private Date lastFailureLogin;
    private Date blockedTo;
    @Column(nullable = true, columnDefinition = "int default 0")
    private int loginFailsSinceLastSuccess;
    @Column(nullable = true)
    private int blockAccountAfter;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getLastSuccesLogin() {
        return lastSuccesLogin;
    }

    public void setLastSuccesLogin(Date lastSuccesLogin) {
        this.lastSuccesLogin = lastSuccesLogin;
    }

    public Date getLastFailureLogin() {
        return lastFailureLogin;
    }

    public void setLastFailureLogin(Date lastFailureLogin) {
        this.lastFailureLogin = lastFailureLogin;
    }

    public int getLoginFailsSinceLastSuccess() {
        return loginFailsSinceLastSuccess;
    }

    public void setLoginFailsSinceLastSuccess(int loginFailsSinceLastSuccess) {
        this.loginFailsSinceLastSuccess = loginFailsSinceLastSuccess;
    }

    public int getBlockAccountAfter() {
        return blockAccountAfter;
    }

    public void setBlockAccountAfter(int blockAccountAfter) {
        this.blockAccountAfter = blockAccountAfter;
    }

    public Date getBlockedTo() {
        return blockedTo;
    }

    public void setBlockedTo(Date blockedTo) {
        this.blockedTo = blockedTo;
    }

    public Date getActualSuccesLogin() {
        return actualSuccesLogin;
    }

    public void setActualSuccesLogin(Date actualSuccesLogin) {
        this.actualSuccesLogin = actualSuccesLogin;
    }

    
}
