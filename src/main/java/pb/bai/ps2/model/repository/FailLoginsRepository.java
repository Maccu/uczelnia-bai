/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pb.bai.ps2.model.repository;

import org.springframework.data.repository.CrudRepository;
import pb.bai.ps2.model.entity.FailLogins;

/**
 *
 * @author Maciek
 */
public interface FailLoginsRepository extends CrudRepository<FailLogins,Integer>{
    
}
